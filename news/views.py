from django.shortcuts import render, HttpResponse, redirect
from .models import News, Category, Contact, NewsComment
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout

# Function for home page
def home(request):
    # fetching all news from databse
    all_news = News.objects.all().order_by('-id')
    context = {
        "all_news":all_news}
    return render(request, 'news/index.html', context)

# Function for displaying single news using id
def newsDetails(request, id):
    news = News.objects.get(id = id)

    # Fetching comment for selected news
    comments = NewsComment.objects.filter(news = news, parent=None)
    context = {"news":news, "comments":comments}
    return render(request, 'news/pages/newsDetails.html', context)

# Function for handeling contact use from
def contact(request):
    # getting users input data and storing in database
    if request.method == "POST":
        name = request.POST.get('name')
        email = request.POST.get('email')
        phone = request.POST.get('phone')
        query = request.POST.get('query')
        contact = Contact(name = name, email = email, phone = phone, query = query)
        contact.save()
        # Displaying success message 
        messages.success(request, "Thanks for contacting we will connect you soon")
    return render(request, 'news/pages/contactus.html')

# Function for handeling login functionality
def handleLogin(request):
    if request.method == "POST":
        username = request.POST['loginusername']
        pwd = request.POST['loginpwd']
        user = authenticate(username = username, password = pwd)
        if user is not None:
            login(request, user)
            messages.success(request, "Successfully loged in")
            return redirect('/')
        else:
            messages.error(request, "Invalid credetials, Please try again")
            return redirect('/')
    else:
        return render(request, 'news/pages/aboutus.html')

# Function for handeling logout functionality
def handleLogout(request):
        logout(request)
        messages.success(request, "Successfully loged out")
        return redirect('/')

# Function for handeling signup functionality
def handleSignup(request):
    # fetching inputtted data and store in databse
    if request.method == "POST":
        username = request.POST['username']
        fname = request.POST['fname']
        lname = request.POST['lname']
        email = request.POST['email']
        pwd = request.POST['pwd']
        cnfpwd = request.POST['cnfpwd']
        user = User.objects.filter(email = email).first()
        if user is not None:
            messages.success(request, "User already exist")
            return redirect('/') 
        if pwd != cnfpwd:
            messages.success(request, "Password does not matched")
            return redirect('/') 
        myuser = User.objects.create_user(username, email, pwd)
        myuser.first_name = fname
        myuser.last_name = lname
        myuser.save()
        messages.success(request, "You account has been created")
        return redirect('/')
    else:
        return HttpResponse('404 - Not found')

# Disaplying all news for single Category
def AllNewsWithCat(request, category):
    category_news = News.objects.filter(category__name=category).order_by('-id')
    context = {"category_news":category_news}
    return render(request, 'news/pages/allNewsWithCat.html', context)

# Function for handeling comment
def postComment(request):
    if request.method=='POST':
        comment = request.POST.get("comment")
        user = request.user
        newsid = request.POST.get("newsid")
        news = News.objects.get(id=newsid)
        comment = NewsComment(comment = comment, user = user, news=news)
        comment.save()
        messages.success(request, "Your comment has been posted")
    return redirect(f'/{news.id}')

# Handeling search result
def search(request):
    query = request.GET.get('search')
    all_news = News.objects.filter(title__icontains=query)
    params = {'all_news':all_news, 'query':query}
    return render(request, 'news/pages/search.html', params)