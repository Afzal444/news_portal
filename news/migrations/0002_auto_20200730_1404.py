# Generated by Django 3.0.5 on 2020-07-30 08:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='image',
            field=models.ImageField(default='', upload_to='news/images'),
        ),
    ]
