from django.urls import path, include
from . import  views

urlpatterns = [
    path('', views.home, name = "home"),
    path('<int:id>', views.newsDetails, name = "news_details"),
    path('news/postcomment', views.postComment, name="postComment"),
    path('contact', views.contact, name = "contact"),
    path('signup', views.handleSignup, name="handleSignup"),
    path('search', views.search, name="search"),
    path('login', views.handleLogin, name="handleLogin"),
    path('logout', views.handleLogout, name="handleLogout"),
    path('news/<category>', views.AllNewsWithCat, name="handleLogout"),
]