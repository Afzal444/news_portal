from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now
# Create your models here.

class Category(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length = 50)
    added_on = models.DateTimeField(auto_now_add=True, blank = True)

    def __str__(self):
        return self.name

class News(models.Model):
    id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length = 50)
    contenet = models.TextField()
    image = models.ImageField(upload_to="news/images", default="")
    slug = models.CharField(max_length = 150)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True)
    added_on = models.DateTimeField(auto_now_add=True, blank = True)

    def __str__(self):
        return self.title

class Contact(models.Model):
    sno = models.IntegerField(primary_key=True)
    name = models.CharField(max_length = 50)
    phone = models.CharField(max_length = 15)
    email = models.CharField(max_length = 50)
    query = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True, blank = True)

    def __str__(self):
        return self.name

class NewsComment(models.Model):
    sno = models.AutoField(primary_key = True)
    comment = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    news = models.ForeignKey(News, models.CASCADE)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True)
    timstamp = models.DateTimeField(default=now)