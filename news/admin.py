from django.contrib import admin

# Register your models here.

from news.models import Category, News, Contact, NewsComment

admin.site.register(Category)
admin.site.register(News)
admin.site.register(Contact)
admin.site.register(NewsComment)