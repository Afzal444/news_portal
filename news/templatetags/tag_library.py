from django import template
from math import ceil

register = template.Library()

@register.filter(name='to_int')
def to_int(value):
    return int(value)

@register.filter(name='devide')
def devide(value, arg):
    return ceil(int(value)/int(arg))